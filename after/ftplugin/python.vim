setlocal expandtab
setlocal autoindent
setlocal nosmarttab
setlocal shiftwidth=2
setlocal tabstop=2
setlocal softtabstop=2
setlocal backspace=indent,eol,start
