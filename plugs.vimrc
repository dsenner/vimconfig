call plug#begin('~/.vim/plugged')

"Plug 'tpope/vim-sensible'
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'sheerun/vim-polyglot'
Plug 'https://github.com/vim-scripts/csv.vim.git'
Plug 'junegunn/vim-easy-align'
Plug 'ctrlpvim/ctrlp.vim'
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

Plug 'editorconfig/editorconfig-vim'
let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']

Plug 'scrooloose/nerdtree'
let NERDTreeIgnore=['\.pyc$', '\~$']

Plug 'airblade/vim-gitgutter'
set updatetime=250

Plug 'stanangeloff/php.vim'

Plug 'slashmili/alchemist.vim'
let g:alchemist_tag_disable = 1

call plug#end()
