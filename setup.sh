#!/bin/bash

if [ -f vimrc ]; then
  mv ~/.vimrc ~/.vimrc.old > /dev/null
  ln -sf vimrc ~/.vimrc
fi

if [ -f gvimrc ]; then
  mv ~/.gvimrc ~/.gvimrc.old > /dev/null
  ln -sf gvimrc ~/.gvimrc
fi
